create table public.cliente(
	id_cliente integer,
	nombre varchar(50),
	apellido varchar(50),
	nro_documento integer,
	tipo_documento varchar(20),
	nacionalidad varchar(30),
	email varchar(50),
	telefono varchar(50),
	nacimiento date,
	constraint cliente_pkey primary key (id_cliente)
);
CREATE SEQUENCE public.cliente_sec;

create table public.concepto_puntos(
	id_concepto integer,
	descripcion varchar(50),
	puntos_req integer,
	constraint concepto_key primary key (id_concepto)
);
CREATE SEQUENCE public.concepto_sec;

create table regla_puntos(    --regla de asignación de puntos -listo
	id_regla integer,
	limite_inferior integer,
	limite_superior integer,
	equivalencia integer,
	id_usuario varchar(50),
	marca_tiempo timestamp,
	constraint regla_pkey primary key (id_regla)
);
CREATE SEQUENCE public.regla_sec;

create table public.vencimiento_puntos(  --parametrización de vencimiento de puntos -listo
	id_vencimiento integer not null,
	inicio_validez DATE,
	fin_validez DATE,
	duracion integer,
	id_usuario varchar(50),
	marca_tiempo timestamp,
	constraint vencimiento_pkey primary key (id_vencimiento)
);
CREATE SEQUENCE public.vencimiento_sec;

create table public.bolsa_puntos( --carga de puntos
	id_bolsa integer not null,
	id_cliente integer not null,
	fecha_asignacion date not null,
	fecha_caducidad date not null,
	p_asignado integer not null,
	p_utilizado integer,
	saldo integer not null,
	monto_operacion integer not null,
	id_usuario varchar(50),
	marca_tiempo timestamp,
	constraint bolsa_pkey primary key (id_bolsa),
	constraint bolsa_fk1 foreign key (id_cliente)
		references public.cliente (id_cliente) match simple
		on update no action on delete no action
);
CREATE SEQUENCE public.bolsa_puntos_sec;

create table public.uso_puntos( --utilización de puntos - listo
	id_uso integer not null,
	id_cliente integer not null,
	p_utilizados integer,
	fecha date,
	id_concepto integer not null,
	id_usuario varchar(50),
	marca_tiempo timestamp,
	constraint uso_pkey primary key (id_uso),
	constraint uso_fk1 foreign key (id_cliente)
		references public.cliente (id_cliente) match simple
		on update no action on delete no action,
	constraint uso_fk2 foreign key (id_concepto)
		references public.concepto_puntos (id_concepto) match simple
		on update no action on delete no action
);
CREATE SEQUENCE public.uso_sec;

create table public.uso_puntos_detalle(
	id_detalle integer not null,
	id_uso integer not null,
	p_utilizados integer,
	id_bolsa integer not null,
	constraint detalle_pkey primary key(id_detalle),
	constraint detalle_fk1 foreign key (id_uso)
		references public.uso_puntos (id_uso) match simple
		on update no action on delete no action,
	constraint detalle_fk2 foreign key (id_bolsa)
		references public.bolsa_puntos (id_bolsa) match simple
		on update no action on delete no action
);
CREATE SEQUENCE public.detalle_sec;


--aca sql adicional, no es necesario si se volvieron a crear las tablas, esto ya esta en el codigo de arriba

alter table vencimiento_puntos add column id_usuario varchar(50);
alter table vencimiento_puntos add column marca_tiempo timestamp;

alter table regla_puntos add column id_usuario varchar(50);
alter table regla_puntos add column marca_tiempo timestamp;

alter table uso_puntos add column id_usuario varchar(50);
alter table uso_puntos add column marca_tiempo timestamp;

alter table bolsa_puntos add column id_usuario varchar(50);
alter table bolsa_puntos add column marca_tiempo timestamp;
