'use strict';

appControllers.controller('MainController', [ '$rootScope', '$scope', '$http', 'authorization',
	function($rootScope, $scope, $http, authorization) {
		$scope.status = 'running...';
		$scope.profile = authorization.profile;
		$scope.isSuperManager = authorization.hasRealmRole('super manager')
		$scope.isCliente = authorization.hasRealmRole('cliente')
		$scope.isBolsaPuntos = authorization.hasRealmRole('bolsa_puntos')
		$scope.isConsultas = authorization.hasRealmRole('consultas')
		$scope.isConceptoPuntos = authorization.hasRealmRole('concepto_puntos')
		$scope.isReglaPuntos = authorization.hasRealmRole('regla_puntos')
		$scope.isUsoPuntosDetalle = authorization.hasRealmRole('uso_puntos_detalle')
		$scope.isUsoPuntos = authorization.hasRealmRole('uso_puntos')
		$scope.isVencimientoPuntos = authorization.hasRealmRole('vencimiento_puntos')
		$scope.isManager = authorization.hasRealmRole('manager')

		$scope.getCliente = function() {
			$http.get("http://localhost:8080/sfc/rest/cliente").then(function(response) {
				//$scope.clientes = angular.fromJson(response.data.lista);
				$scope.clientes = response.data;
			});
		}


		$scope.reportClienteNac = function() {

			$http.get('http://localhost:8080/sfc/rest/consultas/cliente/nacionalidad/' + $scope.nac).then(function(response) {});
		}
		$scope.reportClienteCump = function() {
			$http.get('http://localhost:8080/sfc/rest/consultas/cliente/cumpleanio/' + $scope.cump).then(function(response) {});
		}
		$scope.agregarCliente = function() {
			var data = $scope.nuevoCliente;
			$http.post("http://localhost:8080/sfc/rest/cliente", data);
		}


		$scope.getBolsaPuntos = function() {
			$http.get("http://localhost:8080/sfc/rest/bolsaPuntos").then(function(response) {
				$scope.bolsas = response.data;
			});
		}

		$scope.reportBPcliente = function() {

			$http.get('http://localhost:8080/sfc/rest/consultas/bolsaPuntos/cliente/' + $scope.idCliente).then(function(response) {});
		}
		$scope.reportBPrango = function() {
			$http.get('http://localhost:8080/sfc/rest/consultas/bolsaPuntos/rangoPuntos/' + $scope.rangoPuntos).then(function(response) {});
		}
		$scope.reportBPvencimiento = function() {
			$http.get('http://localhost:8080/sfc/rest/consultas/bolsaPuntos/puntosVencer/' + $scope.dias).then(function(response) {});
		}
		$scope.agregarBolsaPunto = function() {
			var data = $scope.nuevoBolsaPunto;
			$http.post("http://localhost:8080/sfc/rest/bolsaPuntos", data);
		}


		$scope.getVencimientoPuntos = function() {
			$http.get("http://localhost:8080/sfc/rest/vencimientoPuntos").then(function(response) {
				$scope.vencimientoPuntos = angular.fromJson(response.data.lista);
			});
		}
		$scope.agregarVencimientoPunto = function() {
			var data = $scope.nuevoVencimiento;
			$http.post("http://localhost:8080/sfc/rest/vencimientoPuntos", data);
		}

		$scope.getReglaPuntos = function() {
			$http.get("http://localhost:8080/sfc/rest/reglaPuntos").then(function(response) {
				$scope.reglaPuntos = response.data;
			});
		}
		$scope.agregarReglaPuntos = function() {
			var data = $scope.nuevaReglaPunto;
			$http.post("http://localhost:8080/sfc/rest/reglaPuntos", data);
		}

		$scope.getUsoPuntos = function() {
			$http.get("http://localhost:8080/sfc/rest/usoPuntos").then(function(response) {
				$scope.usoPuntos = response.data;
			});
		}
		$scope.agregarUsoPunto = function() {
			var data = $scope.nuevoUsoPunto;
			$http.post("http://localhost:8080/sfc/rest/usoPuntos", data);
		}



		$scope.logout = function() {
			authorization.logout();
		}
	}
]);