angular.module('demo', [])
.controller('Hello', function($scope, $http) {
    $http.get('http://localhost:8080/sfc/rest/cliente').
        then(function(response) {
            $scope.clientes = angular.fromJson(response.data.lista);
        });
});