'use strict';

appControllers.controller('MainController', ['$rootScope', '$scope', '$http', 'authorization',
    function($rootScope, $scope, $http, authorization) {
        $scope.status = 'running...';
        $scope.profile = authorization.profile;
        $scope.isSuperManager = authorization.hasRealmRole('super manager')
        $scope.isManager = authorization.hasRealmRole('manager')
        
        $scope.getAccounts = function() {
        	$http.get("http://localhost:8080/sfc/rest/cliente").then(function(response) {
                $scope.clientes = angular.fromJson(response.data.lista);
            });
        }
        
        $scope.logout = function() {
        	authorization.logout();
        }
    }
]);