package py.com.backend.sfc.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.backend.sfc.modelo.BolsaPuntos;

@Entity
@Table(name="cliente")
public class Cliente implements Serializable{
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_cliente")
    @GeneratedValue(generator="clienteSec")
    @SequenceGenerator(name="clienteSec",sequenceName="cliente_sec",allocationSize=0)
    private Integer idCliente;
    @Basic(optional = false)
    @Column(name = "nombre",length=50)
    private String nombre;
    @Basic(optional = false)
    @Column(name = "apellido",length=50)
    private String apellido;
    @Basic(optional = false)
    @Column(name = "nro_documento",length=50)
    private Integer nroDoc;
    @Basic(optional = false)
    @Column(name = "tipo_documento",length=50)
    private String tipoDoc;
    @Basic(optional = false)
    @Column(name = "nacionalidad",length=50)
    private String nacionalidad;
    @Basic(optional = false)
    @Column(name = "email",length=50)
    private String email;
    @Basic(optional = false)
    @Column(name="telefono",length=50)
    private String telefono;
    @Basic(optional=false)
    @Column(name="nacimiento")
    @Temporal(TemporalType.DATE)
    private Date nacimiento;
    @OneToMany(mappedBy="idCliente")
    @JsonIgnore
    private List<BolsaPuntos> listaBolsaPuntos;
    @Transient
    private List<BolsaPuntos> listaBolsaPuntosTransient;
    public Cliente() {
    	
    }
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Integer getNroDoc() {
		return nroDoc;
	}
	public void setNroDoc(Integer nroDoc) {
		this.nroDoc = nroDoc;
	}
	public String getTipoDoc() {
		return tipoDoc;
	}
	public void setTipoDoc(String tipodoc) {
		this.tipoDoc=tipodoc;
	}
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad=nacionalidad;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email=email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono=telefono;
	}
	public Date getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(Date nacimiento) {
		this.nacimiento=nacimiento;
	}
	public List<BolsaPuntos> getListaBolsaPuntos() {
		return listaBolsaPuntos;
	}
    public void setListaBolsaPuntos(List<BolsaPuntos> listaBolsaPuntos) {
		this.listaBolsaPuntos = listaBolsaPuntos;
	}
    public List<BolsaPuntos> getListaBolsaPuntosTransient() {
		return listaBolsaPuntosTransient;
	}
    public void setListaBolsaPuntosTransient(List<BolsaPuntos> listaBolsaPuntosTransient) {
		this.listaBolsaPuntosTransient = listaBolsaPuntosTransient;
	}
}
