package py.com.backend.sfc.modelo;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

public class ExcelUP {

    private static final String FILE_NAME = "/home/khevin/git/ExcelUsoPuntos.xlsx";

    public static void crear(List<UsoPuntos> args) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Reporte_Excel_UsoPuntos");

        int rowNum = 0;
        System.out.println("Creating excel");
        Iterator<UsoPuntos>it= args.iterator();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Row row = sheet.createRow(rowNum++);
        Cell tc1 = row.createCell(0);
        Cell tc2 = row.createCell(1);
        Cell tc3 = row.createCell(2);
        tc1.setCellValue("Descripcion");
        tc2.setCellValue("Cliente");
        tc3.setCellValue("Fecha");
        while(it.hasNext()) {
			UsoPuntos up= it.next();
			Row row2 = sheet.createRow(rowNum++);
            int colNum = 0;
            Cell cell1 = row2.createCell(colNum++);
            Cell cell2 = row2.createCell(colNum++);
            Cell cell3 = row2.createCell(colNum++);
            cell1.setCellValue((up.getIdConcepto().getDescripcion()));
            cell2.setCellValue(up.getIdCliente().getNombre());
            cell3.setCellValue(df.format(up.getFecha()));
                
            }

        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            workbook.write(outputStream);
            workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done");
    }
}

