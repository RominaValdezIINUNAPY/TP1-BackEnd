package py.com.backend.sfc.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import py.com.backend.sfc.dto.UsoPuntoDTO;
import py.com.backend.sfc.ejb.BolsaPuntosDAO;
import py.com.backend.sfc.ejb.ClienteDAO;
import py.com.backend.sfc.ejb.ConceptoPuntosDAO;
import py.com.backend.sfc.ejb.UsoPuntosDAO;
import py.com.backend.sfc.ejb.UsoPuntosDetalleDAO;
import py.com.backend.sfc.modelo.BolsaPuntos;
import py.com.backend.sfc.modelo.Cliente;
import py.com.backend.sfc.modelo.ConceptoPuntos;
import py.com.backend.sfc.modelo.Email;
import py.com.backend.sfc.modelo.ExcelUP;
import py.com.backend.sfc.modelo.ReporteUP;
import py.com.backend.sfc.modelo.UsoPuntos;
import py.com.backend.sfc.modelo.UsoPuntosDetalle;


@Path("usoPuntos")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class UPRest {
	@Inject
	private UsoPuntosDAO usoPuntosDAO;
	@Inject
	private UsoPuntosDetalleDAO usoDetalleDAO;
	@Inject
	private ClienteDAO clienteDAO;
	@Inject
	private ConceptoPuntosDAO cpDAO;
	@Inject
	private BolsaPuntosDAO bolsaDAO;
	@Context
	protected UriInfo uriInfo;
	@Context
	private HttpServletRequest request;
	
	@GET
	@Path("/")
	public Response listar() throws WebApplicationException{
		
		List<UsoPuntos> listEntity = null;
		Long total = null;
		total = usoPuntosDAO.total();
		listEntity = usoPuntosDAO.lista();
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteUP.crear(listEntity);
			ExcelUP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/{pk}")
	public Response obtener(@PathParam("pk") Integer pk) {
		UsoPuntos entityRespuesta =null;
		entityRespuesta = usoPuntosDAO.get(pk);
		return Response.ok(entityRespuesta).build();
	}

	@POST
	@Path("/")
	public Response crear(UsoPuntoDTO entity) throws WebApplicationException {
		Cliente cliente = clienteDAO.get(entity.getIdCliente());
		
		if(cliente == null)
			return Response.status(Response.Status.BAD_REQUEST).build();
		
		ConceptoPuntos cp = cpDAO.get(entity.getIdUso());
		
		if(cp == null)
			return Response.status(Response.Status.BAD_REQUEST).build();
		
		List<BolsaPuntos> bp = bolsaDAO.lista();

		if(bp==null)
			return Response.status(Response.Status.BAD_REQUEST).build();
		
		List<BolsaPuntos> bolsaCliente = new ArrayList<BolsaPuntos>();
		
		for(BolsaPuntos aux:bp) {
			if(aux.getIdCliente().getIdCliente() == entity.getIdCliente())
				bolsaCliente.add(aux);
		}
		if(bolsaCliente.isEmpty())
			return Response.status(Response.Status.BAD_REQUEST).build();
		
		Integer puntosRequeridos= cp.getPuntosReq();
		Integer saldo=0;
		for(BolsaPuntos aux: bolsaCliente) {
			saldo = aux.getSaldo() - puntosRequeridos;
			if(saldo>=0)
				break;
			else 
				saldo= puntosRequeridos - aux.getSaldo();
		}
		
		if(saldo<0) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}else{
			UsoPuntos uso = new UsoPuntos();
			
			uso.setIdCliente(cliente);
			uso.setPuntosutilizados(cp.getPuntosReq());
			uso.setFecha(new Date());
			uso.setIdConcepto(cp);
			
			//usoPuntosDAO.persist(uso);
			
			List<UsoPuntosDetalle> detalles = new ArrayList<UsoPuntosDetalle>();
			for(BolsaPuntos aux: bolsaCliente) {
				UsoPuntosDetalle upd = new UsoPuntosDetalle();
				upd.setIdUso(uso);
				saldo = aux.getSaldo() - puntosRequeridos;
				if(saldo>=0) {
					aux.setPuntosUtilizados(aux.getPuntosAsignados() - saldo);
					upd.setPuntosUtilizados(puntosRequeridos);
				}
				else {
					aux.setPuntosUtilizados(aux.getPuntosAsignados());
					upd.setPuntosUtilizados(aux.getPuntosAsignados());
				}
				upd.setIdbolsa(aux);
				puntosRequeridos-= aux.getSaldo();
				aux.setSaldo(aux.getPuntosAsignados() - aux.getPuntosUtilizados());
				
				detalles.add(upd);
				
				bolsaDAO.merge(aux);
				
				if(puntosRequeridos <=0)
					break;
			}
			uso.setDetalles(detalles);
			uso.setIdUsuario(request.getUserPrincipal().getName());
			uso.setMarcaTiempo(new Timestamp(System.currentTimeMillis()));
			usoPuntosDAO.persist(uso);
			
			try {
				Email.Send(cliente, cp.getPuntosReq());
			}catch (Exception e) {
				e.printStackTrace();
			}
			UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
					.getAbsolutePath());
			URI resourceUri=null;
			try {
				resourceUri = resourcePathBuilder
						.path(URLEncoder.encode(uso.getIdUso().toString(), "UTF-8")).build();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return Response.created(resourceUri).build();
		}
				
		
	}

	@PUT
	@Path("/")
	public Response modificar(UsoPuntos entity) throws WebApplicationException {
		entity.setIdUsuario(request.getUserPrincipal().getName());
		entity.setMarcaTiempo(new Timestamp(System.currentTimeMillis()));
		usoPuntosDAO.merge(entity);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{pk}")
	public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {
		usoPuntosDAO.delete(pk);
		return Response.ok().build();
	}
}
