package py.com.backend.sfc.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.backend.sfc.modelo.VencimientoPuntos;

@Stateless
public class VencimientoPuntosDAO {
	@PersistenceContext(unitName="sfcPU")
    private EntityManager em;
	
	protected EntityManager getEm() {
		return em;
	}	
	
    public VencimientoPuntos get(Integer id) {
        return em.find(VencimientoPuntos.class, id);
    }    
    
    public void persist(VencimientoPuntos entity){
        getEm().persist(entity);
    }    
    public VencimientoPuntos merge(VencimientoPuntos entity){
        return (VencimientoPuntos) getEm().merge(entity);
    }    
    public void delete(Integer id){
    	VencimientoPuntos entity = this.get(id);
        this.getEm().remove(entity);
    }    
    public void delete(VencimientoPuntos entity){
    		this.delete(entity.getIdVencimiento());
    }  
    @SuppressWarnings("unchecked")
	public List<VencimientoPuntos> lista() {
    		Query q = getEm().createQuery(
    			"SELECT rp FROM VencimientoPuntos rp");
    		return (List<VencimientoPuntos>) q.getResultList();
    }
    public Long total() {
    		Query q = getEm().createQuery(
    			"Select Count(rp) from VencimientoPuntos rp");
    		return (Long) q.getSingleResult();
    }
}
