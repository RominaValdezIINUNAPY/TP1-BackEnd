package py.com.backend.sfc.modelo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Iterator;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfDocument;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ReporteUP {

	public static void crear(List<UsoPuntos> args) throws FileNotFoundException, DocumentException {
	     // 1. Create document
        Document document = new Document();

        // 2. Create PdfWriter
        PdfWriter.getInstance(document, new FileOutputStream("/home/khevin/git/result.pdf"));

  
        document.open();
		PdfPTable table = new PdfPTable(new float[] { 2, 1, 2 });
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell("Concepto");
		table.addCell("Nombre");
		table.addCell("Fecha");
		table.setHeaderRows(1);
		PdfPCell[] cells = table.getRow(0).getCells();
		for (int j = 0; j < cells.length; j++) {
			cells[j].setBackgroundColor(BaseColor.GRAY);
		}
		Iterator<UsoPuntos> it= args.iterator();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		while(it.hasNext()) {
			UsoPuntos up= it.next();
			table.addCell(up.getIdConcepto().getDescripcion());
			table.addCell(up.getIdCliente().getNombre());
			table.addCell(df.format(up.getFecha()));
		}
		document.add(table);
		document.close();
		System.out.println("Done");
	}
}
