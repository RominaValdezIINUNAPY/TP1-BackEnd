package py.com.backend.sfc.modelo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="uso_puntos_detalle")
public class UsoPuntosDetalle implements Serializable{
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_detalle")
    @GeneratedValue(generator="usoDetalle")
    @SequenceGenerator(name="usoDetalle",sequenceName="uso_detalle",allocationSize=0)
    private Integer idDetalle;
    @JoinColumn(name="id_uso", referencedColumnName="id_uso")
	@ManyToOne(optional=false)
	private UsoPuntos idUso;
    @Basic(optional = false)
    @Column(name = "p_utilizados")
    private Integer puntosUtilizados;
    @JoinColumn(name="id_bolsa", referencedColumnName="id_bolsa")
	@ManyToOne(optional=false)
	private BolsaPuntos idbolsa;
    
	public Integer getIdDetalle() {
		return idDetalle;
	}
	public void setIdDetalle(Integer idDetalle) {
		this.idDetalle = idDetalle;
	}
	public UsoPuntos getIdUso() {
		return idUso;
	}
	public void setIdUso(UsoPuntos idUso) {
		this.idUso = idUso;
	}
	public Integer getPuntosUtilizados() {
		return puntosUtilizados;
	}
	public void setPuntosUtilizados(Integer puntosUtilizados) {
		this.puntosUtilizados = puntosUtilizados;
	}
	public BolsaPuntos getIdbolsa() {
		return idbolsa;
	}
	public void setIdbolsa(BolsaPuntos idbolsa) {
		this.idbolsa = idbolsa;
	}

}
