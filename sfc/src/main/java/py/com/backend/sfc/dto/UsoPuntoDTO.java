package py.com.backend.sfc.dto;

public class UsoPuntoDTO {
	private Integer idCliente;
	private Integer idUso;
	
	
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public Integer getIdUso() {
		return idUso;
	}
	public void setIdUso(Integer idUso) {
		this.idUso = idUso;
	}
}
