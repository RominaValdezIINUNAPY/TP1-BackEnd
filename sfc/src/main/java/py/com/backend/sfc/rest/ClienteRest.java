package py.com.backend.sfc.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import py.com.backend.sfc.ejb.ClienteDAO;
import py.com.backend.sfc.modelo.Cliente;
import py.com.backend.sfc.modelo.ExcelCliente;
import py.com.backend.sfc.modelo.ReporteClientes;
import py.com.backend.sfc.modelo.ReporteUP;

@Path("cliente")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ClienteRest {
	@Inject
	private ClienteDAO clienteDAO;
	@Context
	protected UriInfo uriInfo;
	@Context
	private HttpServletRequest request;
	
	
	@GET
	@Path("/")
	public Response listar() throws WebApplicationException{
		Principal userPrincipal = request.getUserPrincipal();
		System.out.println("ACAAAAAAAA ESTAA: " + userPrincipal.getName());
		List<Cliente> listEntity = null;
		Long total = null;
		total = clienteDAO.total();
		listEntity = clienteDAO.lista();
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		try {
			ReporteClientes.crear(listEntity);
			ExcelCliente.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok(mapaResultado).build();
		
	}
	
	@GET
	@Path("/{pk}")
	public Response obtener(@PathParam("pk") Integer pk) {
		Cliente entityRespuesta =null;
		entityRespuesta = clienteDAO.get(pk);
		return Response.ok(entityRespuesta).build();
	}

	@POST
	@Path("/")
	public Response crear(Cliente entity) throws WebApplicationException {
		
		clienteDAO.persist(entity);
		
		UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
				.getAbsolutePath());
		URI resourceUri=null;
		try {
			resourceUri = resourcePathBuilder
					.path(URLEncoder.encode(entity.getIdCliente().toString(), "UTF-8")).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.created(resourceUri).build();
	}

	@PUT
	@Path("/")
	public Response modificar(Cliente entity) throws WebApplicationException {
		clienteDAO.merge(entity);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{pk}")
	public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {
		
		clienteDAO.delete(pk);
		return Response.ok().build();

	}
}
