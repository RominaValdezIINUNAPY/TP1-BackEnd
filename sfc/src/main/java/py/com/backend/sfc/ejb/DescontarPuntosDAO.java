//package py.com.backend.sfc.ejb;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import javax.ejb.AccessTimeout;
//import javax.ejb.Schedule;
//import javax.ejb.Singleton;
//import javax.ejb.Startup;
//import javax.ejb.Timeout;
//import javax.persistence.EntityManager;
//import javax.persistence.PersistenceContext;
//import javax.persistence.Query;
//
//import py.com.backend.sfc.modelo.BolsaPuntos;
//
//@Singleton
//@Startup
//public class DescontarPuntosDAO {
//	@PersistenceContext(unitName="sfcPU")
//    private EntityManager em;
//	
//	protected EntityManager getEm() {
//		return em;
//	}	
//    public BolsaPuntos merge(BolsaPuntos entity){
//        return (BolsaPuntos) getEm().merge(entity);
//    }     
//    
//    @SuppressWarnings("unchecked")
//	public List<BolsaPuntos> listaPorPuntosVencidos() {
//		Query q = getEm().createQuery(
//			"SELECT bp FROM BolsaPuntos bp where fecha_caducidad=current_date");
//		return (List<BolsaPuntos>) q.getResultList();
//	}
//
//    //Esto es para hacer cada 10 segundos
//	//@Schedule(second = "*/10", minute = "*", hour = "*")
//    //Esto es para cada 1 dia a la media noche
//    @Schedule(hour="*", minute="*")
//    public void descontar() throws InterruptedException {
//		List<BolsaPuntos> lista = null;
//		lista = listaPorPuntosVencidos();
//		
//		for(BolsaPuntos aux:lista) {
//			aux.setSaldo(0);
//			merge(aux);
//		}
//		
//        System.out.println("Tarea descontar puntos realizada");
//        
//    }
//}
