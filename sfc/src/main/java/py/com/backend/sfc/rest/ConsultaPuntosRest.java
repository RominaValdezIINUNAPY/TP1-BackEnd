package py.com.backend.sfc.rest;


import java.net.URI;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import py.com.backend.sfc.dto.ConsultaPuntosDTO;
import py.com.backend.sfc.ejb.ReglaPuntosDAO;


@Path("consultaPuntos")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ConsultaPuntosRest {
	
	private Integer puntos; 
	private String mensaje;
	
	@Inject
	private ReglaPuntosDAO reglaPuntosDAO;
		
	@Context
	protected UriInfo uriInfo;
	

	@POST
	@Path("/")
	public String consultarPuntos(ConsultaPuntosDTO dto){
		
		
		puntos= dto.getMonto() / reglaPuntosDAO.get(1).getEquivalencia();
		
		mensaje= "La cantidad de Puntos equivalentes al monto "+dto.getMonto()+" es: "+puntos;
		
		return mensaje;

		

	}
	
	


}
