package py.com.backend.sfc.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.backend.sfc.modelo.Cliente;

@Stateless
public class ClienteDAO {
	@PersistenceContext(unitName="sfcPU")
    private EntityManager em;
	
	protected EntityManager getEm() {
		return em;
	}	
	
    public Cliente get(Integer id) {
        return em.find(Cliente.class, id);
    }    
    
    public void persist(Cliente entity){
        getEm().persist(entity);
    }    
    public Cliente merge(Cliente entity){
        return (Cliente) getEm().merge(entity);
    }    
    public void delete(Integer id){
        Cliente entity = this.get(id);
        this.getEm().remove(entity);
    }    
    public void delete(Cliente entity){
    		this.delete(entity.getIdCliente());
    }  
    @SuppressWarnings("unchecked")
	public List<Cliente> lista() {
    		Query q = getEm().createQuery(
    			"SELECT c FROM Cliente c");
    		return (List<Cliente>) q.getResultList();
    }
    public Long total() { 
    		Query q = getEm().createQuery(
    			"Select Count(c) from Cliente c");
    		return (Long) q.getSingleResult();
    }
    @SuppressWarnings("unchecked")
	public List<Cliente> listaPorNacionalidad(String nac) {
		Query q = getEm().createQuery(
			"SELECT c FROM Cliente c where nacionalidad = :nac ")
				.setParameter("nac", nac);
		return (List<Cliente>) q.getResultList();
	}
    public Long totalPorNacionalidad(String nac) {
		Query q = getEm().createQuery(
			"Select Count(c) from Cliente c where nacionalidad = :nac ")
				.setParameter("nac", nac);
		return (Long) q.getSingleResult();
	}
    @SuppressWarnings("unchecked")
	public List<Cliente> listaPorCumpleanio(Date cumple) {
		Query q = getEm().createQuery(
			"SELECT c FROM Cliente c where nacimiento = :cumple ")
				.setParameter("cumple", cumple);
		return (List<Cliente>) q.getResultList();
	}
    public Long totalPorCumpleanio(Date cumple) {
		Query q = getEm().createQuery(
			"Select Count(c) from Cliente c where nacimiento = :cumple ")
				.setParameter("cumple", cumple);
		return (Long) q.getSingleResult();
	}
}
