package py.com.backend.sfc.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="bolsa_puntos")
public class BolsaPuntos implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional=false)
	@Column(name="id_bolsa")
	@GeneratedValue(generator="bolsaPuntosSec")
	@SequenceGenerator(name="bolsaPuntosSec",sequenceName="bolsa_puntos_sec",allocationSize=0)
	private Integer idBolsa;
	@JoinColumn(name="id_cliente", referencedColumnName="id_cliente")
	@ManyToOne(optional=false)
	private Cliente idCliente;
	@Basic(optional=false)
	@Column(name="fecha_asignacion",length=50)
	@Temporal(TemporalType.DATE)
	private Date fechaAsignacion;
	@Basic(optional=false)
	@Column(name="fecha_caducidad",length=50)
	@Temporal(TemporalType.DATE)
	private Date fechaCaducidad;
	@Basic(optional=false)
	@Column(name="p_asignado",length=10)
	private Integer puntosAsignados;
	@Basic(optional=false)
	@Column(name="p_utilizado",length=10)
	private Integer puntosUtilizados;
	@Basic(optional=false)
	@Column(name="saldo",length=20)
	private Integer saldo;
	@Basic(optional=false)
	@Column(name="monto_operacion",length=20)
	private Integer montoOperacion;
	@Basic(optional = false)
    @Column(name = "id_usuario",length=50)
    private String idUsuario;
    @Basic(optional = false)
    @Column(name = "marca_tiempo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date marcaTiempo;
	
	public Integer getIdBolsa() {
		return idBolsa;
	}
	public void setIdBolsa(Integer idBolsa) {
		this.idBolsa = idBolsa;
	}
	public Cliente getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Cliente cliente) {
		this.idCliente = cliente;
	}
	public Date getFechaAsignacion() {
		return fechaAsignacion;
	}
	public void setFechaAsignacion(Date fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}
	public Date getFechaCaducidad() {
		return fechaCaducidad;
	}
	public void setFechaCaducidad(Date fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}
	public Integer getPuntosAsignados() {
		return puntosAsignados;
	}
	public void setPuntosAsignados(Integer puntosAsignados) {
		this.puntosAsignados = puntosAsignados;
	}
	public Integer getPuntosUtilizados() {
		return puntosUtilizados;
	}
	public void setPuntosUtilizados(Integer puntosUtilizados) {
		this.puntosUtilizados = puntosUtilizados;
	}
	public Integer getSaldo() {
		return saldo;
	}
	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}
	public Integer getMontoOperacion() {
		return montoOperacion;
	}
	public void setMontoOperacion(Integer montoOperacion) {
		this.montoOperacion = montoOperacion;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Date getMarcaTiempo() {
		return marcaTiempo;
	}
	public void setMarcaTiempo(Date marcaTiempo) {
		this.marcaTiempo = marcaTiempo;
	}
	@Override
	public BolsaPuntos clone() {
		BolsaPuntos bp=new BolsaPuntos();
		bp.setMontoOperacion(this.montoOperacion);
		bp.setFechaAsignacion(this.getFechaAsignacion());
		bp.setFechaCaducidad(this.getFechaCaducidad());
		bp.setPuntosAsignados(this.puntosAsignados);
		bp.setSaldo(this.saldo);
		bp.setIdBolsa(this.idBolsa);
		return bp;
	}
	
}
