package py.com.backend.sfc.modelo;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class ReporteBP {
	
	public static void crear(List<BolsaPuntos> args) throws FileNotFoundException, DocumentException {
	     // 1. Create document
       Document document = new Document();

       // 2. Create PdfWriter
       PdfWriter.getInstance(document, new FileOutputStream("/home/khevin/git/result2.pdf"));

 
       document.open();
		PdfPTable table = new PdfPTable(new float[] { 2, 1 });
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell("Cliente");
		table.addCell("Puntos");
		table.setHeaderRows(1);
		PdfPCell[] cells = table.getRow(0).getCells();
		for (int j = 0; j < cells.length; j++) {
			cells[j].setBackgroundColor(BaseColor.GRAY);
		}
		Iterator<BolsaPuntos> it= args.iterator();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		while(it.hasNext()) {
			BolsaPuntos bp= it.next();
			table.addCell(bp.getIdCliente().getNombre());
			table.addCell(String.valueOf(bp.getSaldo()));
		}
		document.add(table);
		document.close();
		System.out.println("Done");
	}
}
