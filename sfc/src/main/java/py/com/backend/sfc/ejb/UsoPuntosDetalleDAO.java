package py.com.backend.sfc.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.backend.sfc.modelo.UsoPuntosDetalle;

@Stateless
public class UsoPuntosDetalleDAO {
	@PersistenceContext(unitName="sfcPU")
    private EntityManager em;
	protected EntityManager getEm() {
		return em;
	}
	public UsoPuntosDetalle get(Integer id) {
        return em.find(UsoPuntosDetalle.class, id);
    }    
    
    public void persist(UsoPuntosDetalle entity){
        getEm().persist(entity);
    }    
    public UsoPuntosDetalle merge(UsoPuntosDetalle entity){
        return (UsoPuntosDetalle) getEm().merge(entity);
    }    
    public void delete(Integer id){
    	UsoPuntosDetalle entity = this.get(id);
        this.getEm().remove(entity);
    }    
    public void delete(UsoPuntosDetalle entity){
    		this.delete(entity.getIdDetalle());
    }  
    @SuppressWarnings("unchecked")
	public List<UsoPuntosDetalle> lista() {
    		Query q = getEm().createQuery(
    			"SELECT upd FROM UsoPuntosDetalle upd");
    		return (List<UsoPuntosDetalle>) q.getResultList();
    }
    public Long total() {
    		Query q = getEm().createQuery(
    			"Select Count(upd) from UsoPuntosDetalle upd");
    		return (Long) q.getSingleResult();
    }
}
