package py.com.backend.sfc.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import py.com.backend.sfc.ejb.*;
import py.com.backend.sfc.modelo.*;

@Path("consultas")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class ConsultasRest {
	@Inject
	private UsoPuntosDAO usoPuntosDAO;
	@Inject
	private BolsaPuntosDAO bolsaPuntosDAO;
	@Inject
	private ClienteDAO clienteDAO;
	@Context
	protected UriInfo uriInfo;
	
	@GET
	@Path("/usoPuntos/concepto/{pk}")
	public Response obtenerUsoPuntosConcepto(@PathParam("pk") Integer pk) {
		List<UsoPuntos> listEntity =null;
		Long total = null;
		total = usoPuntosDAO.totalPorConcepto(pk);
		listEntity = usoPuntosDAO.listaPorConcepto(pk);
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteUP.crear(listEntity);
			ExcelUP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/usoPuntos/fecha/{fecha}")
	public Response obtenerUsoPuntosFecha(@PathParam("fecha") String fecha) throws ParseException {
		List<UsoPuntos> listEntity =null;
		Long total = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		total = usoPuntosDAO.totalPorFechaUso(format.parse(fecha));
		listEntity = usoPuntosDAO.listaPorFechaUso(format.parse(fecha));
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteUP.crear(listEntity);
			ExcelUP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/usoPuntos/cliente/{pk}")
	public Response obtenerUsoPuntosCliente(@PathParam("pk") Integer pk) {
		List<UsoPuntos> listEntity =null;
		Long total = null;
		total = usoPuntosDAO.totalPorCliente(pk);
		listEntity = usoPuntosDAO.listaPorCliente(pk);
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteUP.crear(listEntity);
			ExcelUP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/bolsaPuntos/cliente/{pk}")
	public Response obtenerBolsaPuntosCliente(@PathParam("pk") Integer pk) {
		List<BolsaPuntos> listEntity =null;
		Long total = null;
		total = bolsaPuntosDAO.totalPorCliente(pk);
		listEntity = bolsaPuntosDAO.listaPorCliente(pk);
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteBP.crear(listEntity);
			ExcelBP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/bolsaPuntos/rangoPuntos/{inicio}-{fin}")
	public Response obtenerBolsaPuntosRangoPuntos(@PathParam("inicio") Integer inicio, @PathParam("fin") Integer fin) {
		List<BolsaPuntos> listEntity =null;
		Long total = null;
		total = bolsaPuntosDAO.totalPorRangoPuntos(inicio, fin);
		listEntity = bolsaPuntosDAO.listaPorRangoPuntos(inicio, fin);
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteBP.crear(listEntity);
			ExcelBP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/bolsaPuntos/puntosVencer/{dias}")
	public Response obtenerClientePuntosVencer(@PathParam("dias") Integer dias) {
		List<BolsaPuntos> listEntity =null;
		Long total = null;
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, dias);
		dt = c.getTime();
		total = bolsaPuntosDAO.totalPorPuntosVencer(dt);
		listEntity = bolsaPuntosDAO.listaPorPuntosVencer(dt);
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteBP.crear(listEntity);
			ExcelBP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/cliente/nacionalidad/{nac}")
	public Response obtenerClienteNacionalidad(@PathParam("nac") String nac) {
		List<Cliente> listEntity =null;
		Long total = null;
		total = clienteDAO.totalPorNacionalidad(nac);
		listEntity = clienteDAO.listaPorNacionalidad(nac);
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteClientes.crear(listEntity);
			ExcelCliente.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}
	
	@GET
	@Path("/cliente/cumpleanio/{cumple}")
	public Response obtenerClienteCumpleanio(@PathParam("cumple") String cumple) throws ParseException {
		List<Cliente> listEntity =null;
		Long total = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		total = clienteDAO.totalPorCumpleanio(format.parse(cumple));
		listEntity = clienteDAO.listaPorCumpleanio(format.parse(cumple));
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteClientes.crear(listEntity);
			ExcelCliente.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return Response.ok(mapaResultado).build();
	}

}
