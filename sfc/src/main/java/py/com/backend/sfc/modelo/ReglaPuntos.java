package py.com.backend.sfc.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="regla_puntos")
public class ReglaPuntos implements Serializable{
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_regla")
    @GeneratedValue(generator="reglaSec")
    @SequenceGenerator(name="reglaSec",sequenceName="regla_sec",allocationSize=0)
    private Integer idRegla;
    @Basic(optional = false)
    @Column(name = "limite_inferior")
    private Integer limiteInferior;
    @Basic(optional = false)
    @Column(name = "limite_superior")
    private Integer limiteSuperior;
    @Basic(optional = false)
    @Column(name = "equivalencia")
    private Integer equivalencia;
    @Basic(optional = false)
    @Column(name = "id_usuario",length=50)
    private String idUsuario;
    @Basic(optional = false)
    @Column(name = "marca_tiempo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date marcaTiempo;
    
    public ReglaPuntos() {
    	
    }
	public Integer getIdRegla() {
		return idRegla;
	}
	public void setIdRegla(Integer idRegla) {
		this.idRegla = idRegla;
	}
	public Integer getLimiteInferior() {
		return limiteInferior;
	}
	public void setLimiteInferior(Integer limiteInferior) {
		this.limiteInferior = limiteInferior;
	}
	public Integer getLimiteSuperior() {
		return limiteSuperior;
	}
	public void setLimiteSuperior(Integer limiteSuperior) {
		this.limiteSuperior = limiteSuperior;
	}
	public Integer getEquivalencia() {
		return equivalencia;
	}
	public void setEquivalencia(Integer equivalencia) {
		this.equivalencia = equivalencia;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Date getMarcaTiempo() {
		return marcaTiempo;
	}
	public void setMarcaTiempo(Date marcaTiempo) {
		this.marcaTiempo = marcaTiempo;
	}
	
	
}
