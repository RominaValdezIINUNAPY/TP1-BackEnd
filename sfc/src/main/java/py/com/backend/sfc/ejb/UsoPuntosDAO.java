package py.com.backend.sfc.ejb;

import java.util.List;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.backend.sfc.modelo.UsoPuntos;

@Stateless
public class UsoPuntosDAO {
	@PersistenceContext(unitName="sfcPU")
    private EntityManager em;
	protected EntityManager getEm() {
		return em;
	}
	public UsoPuntos get(Integer id) {
        return em.find(UsoPuntos.class, id);
    }    
    
    public void persist(UsoPuntos entity){
        getEm().persist(entity);
    }    
    public UsoPuntos merge(UsoPuntos entity){
        return (UsoPuntos) getEm().merge(entity);
    }    
    public void delete(Integer id){
    	UsoPuntos entity = this.get(id);
        this.getEm().remove(entity);
    }    
    public void delete(UsoPuntos entity){
    		this.delete(entity.getIdUso());
    }  
    @SuppressWarnings("unchecked")
	public List<UsoPuntos> lista() {
    		Query q = getEm().createQuery(
    			"SELECT up FROM UsoPuntos up");
    		return (List<UsoPuntos>) q.getResultList();
    }
    public Long total() {
    		Query q = getEm().createQuery(
    			"Select Count(up) from UsoPuntos up");
    		return (Long) q.getSingleResult();
    }
    @SuppressWarnings("unchecked")
	public List<UsoPuntos> listaPorConcepto(Integer pk) {
		Query q = getEm().createQuery(
			"SELECT up FROM UsoPuntos up where id_concepto=:pk")
				.setParameter("pk", pk);
		return (List<UsoPuntos>) q.getResultList();
	}
    public Long totalPorConcepto(Integer pk) {
		Query q = getEm().createQuery(
			"Select Count(up) from UsoPuntos up where id_concepto=:pk")
				.setParameter("pk", pk);
		return (Long) q.getSingleResult();
	}
    @SuppressWarnings("unchecked")
	public List<UsoPuntos> listaPorFechaUso(Date fecha) {
		Query q = getEm().createQuery(
			"SELECT up FROM UsoPuntos up where fecha=:fecha")
				.setParameter("fecha", fecha);
		return (List<UsoPuntos>) q.getResultList();
	}
    public Long totalPorFechaUso(Date fecha) {
		Query q = getEm().createQuery(
			"Select Count(up) from UsoPuntos up where fecha=:fecha")
				.setParameter("fecha", fecha);
		return (Long) q.getSingleResult();
	}
    @SuppressWarnings("unchecked")
	public List<UsoPuntos> listaPorCliente(Integer pk) {
		Query q = getEm().createQuery(
			"SELECT up FROM UsoPuntos up where id_cliente=:pk")
				.setParameter("pk", pk);
		return (List<UsoPuntos>) q.getResultList();
	}
    public Long totalPorCliente(Integer pk) {
		Query q = getEm().createQuery(
			"Select Count(up) from UsoPuntos up where id_cliente=:pk")
				.setParameter("pk", pk);
		return (Long) q.getSingleResult();
	}
}
