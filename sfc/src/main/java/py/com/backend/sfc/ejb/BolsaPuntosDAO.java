package py.com.backend.sfc.ejb;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.backend.sfc.modelo.BolsaPuntos;
import py.com.backend.sfc.modelo.UsoPuntos;

@Stateless
public class BolsaPuntosDAO {
	@PersistenceContext(unitName="sfcPU")
    private EntityManager em;
	
	protected EntityManager getEm() {
		return em;
	}	
	
    public BolsaPuntos get(Integer id) {
        return em.find(BolsaPuntos.class, id);
    }    
    
    public void persist(BolsaPuntos entity){
        getEm().persist(entity);
    }    
    public BolsaPuntos merge(BolsaPuntos entity){
        return (BolsaPuntos) getEm().merge(entity);
    }    
    public void delete(Integer id){
        BolsaPuntos entity = this.get(id);
        this.getEm().remove(entity);
    }    
    public void delete(BolsaPuntos entity){
    		this.delete(entity.getIdBolsa());
    }  
    @SuppressWarnings("unchecked")
	public List<BolsaPuntos> lista() {
    		Query q = getEm().createQuery(
    			"SELECT bp FROM BolsaPuntos bp");
    		return (List<BolsaPuntos>) q.getResultList();
    }
    public Long total() { 
    		Query q = getEm().createQuery(
    			"Select Count(bp) from BolsaPuntos bp");
    		return (Long) q.getSingleResult();
    }
    @SuppressWarnings("unchecked")
	public List<BolsaPuntos> listaPorCliente(Integer pk) {
		Query q = getEm().createQuery(
			"SELECT bp FROM BolsaPuntos bp where id_cliente=:pk")
				.setParameter("pk", pk);
		return (List<BolsaPuntos>) q.getResultList();
	}
    public Long totalPorCliente(Integer pk) {
		Query q = getEm().createQuery(
			"Select Count(bp) from BolsaPuntos bp where id_cliente=:pk")
				.setParameter("pk", pk);
		return (Long) q.getSingleResult();
	}
    @SuppressWarnings("unchecked")
	public List<BolsaPuntos> listaPorRangoPuntos(Integer inicio, Integer fin) {
		Query q = getEm().createQuery(
			"SELECT bp FROM BolsaPuntos bp where saldo>=:inicio and saldo<=:fin")
				.setParameter("inicio", inicio)
				.setParameter("fin", fin);
		return (List<BolsaPuntos>) q.getResultList();
	}
    public Long totalPorRangoPuntos(Integer inicio, Integer fin) {
		Query q = getEm().createQuery(
			"Select Count(bp) from BolsaPuntos bp where saldo>=:inicio and saldo<=:fin")
				.setParameter("inicio", inicio)
				.setParameter("fin", fin);
		return (Long) q.getSingleResult();
	}
    @SuppressWarnings("unchecked")
	public List<BolsaPuntos> listaPorPuntosVencer(Date dt) {
		Query q = getEm().createQuery(
			"SELECT bp FROM BolsaPuntos bp where fecha_caducidad >=NOW() and fecha_caducidad<=:dt")
				.setParameter("dt",dt);
		return (List<BolsaPuntos>) q.getResultList();
	}
    public Long totalPorPuntosVencer(Date dt) {
		Query q = getEm().createQuery(
			"Select Count(bp) from BolsaPuntos bp where fecha_caducidad >=NOW() and fecha_caducidad<=:dt")
				.setParameter("dt", dt);
		return (Long) q.getSingleResult();
	}
}