package py.com.backend.sfc.modelo;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="concepto_puntos")
public class ConceptoPuntos implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_concepto")
    @GeneratedValue(generator="conceptoSec")
    @SequenceGenerator(name="conceptoSec",sequenceName="concepto_sec",allocationSize=0)
    private Integer idConcepto;
    @Basic(optional = false)
    @Column(name = "descripcion",length=50)
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "puntos_req")
    private Integer puntosReq;
    
    public ConceptoPuntos() {
    	
    }
	public Integer getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(Integer idConcepto) {
		this.idConcepto = idConcepto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getPuntosReq() {
		return puntosReq;
	}
	public void setPuntosReq(Integer puntosReq) {
		this.puntosReq = puntosReq;
	}
}
