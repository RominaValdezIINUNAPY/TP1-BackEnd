package py.com.backend.sfc.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.backend.sfc.modelo.ConceptoPuntos;

@Stateless
public class ConceptoPuntosDAO {
	@PersistenceContext(unitName="sfcPU")
    private EntityManager em;
	
	protected EntityManager getEm() {
		return em;
	}	
	
    public ConceptoPuntos get(Integer id) {
        return em.find(ConceptoPuntos.class, id);
    }    
    
    public void persist(ConceptoPuntos entity){
        getEm().persist(entity);
    }    
    public ConceptoPuntos merge(ConceptoPuntos entity){
        return (ConceptoPuntos) getEm().merge(entity);
    }    
    public void delete(Integer id){
    	ConceptoPuntos entity = this.get(id);
        this.getEm().remove(entity);
    }    
    public void delete(ConceptoPuntos entity){
    		this.delete(entity.getIdConcepto());
    }  
    @SuppressWarnings("unchecked")
	public List<ConceptoPuntos> lista() {
    		Query q = getEm().createQuery(
    			"SELECT cp FROM ConceptoPuntos cp");
    		return (List<ConceptoPuntos>) q.getResultList();
    }
    public Long total() {
    		Query q = getEm().createQuery(
    			"Select Count(cp) from ConceptoPuntos cp");
    		return (Long) q.getSingleResult();
    }
}
