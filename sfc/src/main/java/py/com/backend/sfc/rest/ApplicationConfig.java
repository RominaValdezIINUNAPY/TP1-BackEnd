package py.com.backend.sfc.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest/")
public class ApplicationConfig extends Application{

}
