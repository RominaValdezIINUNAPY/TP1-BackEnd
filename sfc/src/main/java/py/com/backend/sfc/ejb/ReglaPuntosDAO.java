package py.com.backend.sfc.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import py.com.backend.sfc.modelo.ReglaPuntos;

@Stateless
public class ReglaPuntosDAO {
	@PersistenceContext(unitName="sfcPU")
    private EntityManager em;
	
	protected EntityManager getEm() {
		return em;
	}	
	
    public ReglaPuntos get(Integer id) {
        return em.find(ReglaPuntos.class, id);
    }    
    
    public void persist(ReglaPuntos entity){
        getEm().persist(entity);
    }    
    public ReglaPuntos merge(ReglaPuntos entity){
        return (ReglaPuntos) getEm().merge(entity);
    }    
    public void delete(Integer id){
    	ReglaPuntos entity = this.get(id);
        this.getEm().remove(entity);
    }    
    public void delete(ReglaPuntos entity){
    		this.delete(entity.getIdRegla());
    }  
    @SuppressWarnings("unchecked")
	public List<ReglaPuntos> lista() {
    		Query q = getEm().createQuery(
    			"SELECT rp FROM ReglaPuntos rp");
    		return (List<ReglaPuntos>) q.getResultList();
    }
    public Long total() {
    		Query q = getEm().createQuery(
    			"Select Count(rp) from ReglaPuntos rp");
    		return (Long) q.getSingleResult();
    }
}
