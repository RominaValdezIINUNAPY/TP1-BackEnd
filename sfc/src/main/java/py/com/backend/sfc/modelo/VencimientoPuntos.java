package py.com.backend.sfc.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="vencimiento_puntos")
public class VencimientoPuntos implements Serializable {
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_vencimiento")
    @GeneratedValue(generator="vencimientoSec")
    @SequenceGenerator(name="vencimientoSec",sequenceName="vencimiento_sec",allocationSize=0)
    private Integer idVencimiento;
    @Basic(optional = false)
    @Column(name = "inicio_validez")
    @Temporal(TemporalType.DATE)
    private Date inicioValidez;
    @Basic(optional = false)
    @Column(name = "fin_validez")
    @Temporal(TemporalType.DATE)
    private Date finValidez;
    @Basic(optional = false)
    @Column(name = "duracion") //En días
    private Integer duracion;
    @Basic(optional = false)
    @Column(name = "id_usuario",length=50)
    private String idUsuario;
    @Basic(optional = false)
    @Column(name = "marca_tiempo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date marcaTiempo;
    
    public VencimientoPuntos() {
    	
    }
	public Integer getIdVencimiento() {
		return idVencimiento;
	}
	public void setIdVencimiento(Integer idVencimiento) {
		this.idVencimiento = idVencimiento;
	}
	public Date getInicioValidez() {
		return inicioValidez;
	}
	public void setInicioValidez(Date inicioValidez) {
		final long hours12 = 12L * 60L * 60L * 1000L;
		Date d = new Date(inicioValidez.getTime() + hours12);
		this.inicioValidez = d;
	}
	public Date getFinValidez() {
		return finValidez;
	}
	public void setFinValidez(Date finValidez) {
		final long hours12 = 12L * 60L * 60L * 1000L;
		Date d = new Date(finValidez.getTime() + hours12);
		this.finValidez = d;
	}
	public Integer getDuracion() {
		return duracion;
	}
	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Date getMarcaTiempo() {
		return marcaTiempo;
	}
	public void setMarcaTiempo(Date marcaTiempo) {
		this.marcaTiempo = marcaTiempo;
	}
	
}
