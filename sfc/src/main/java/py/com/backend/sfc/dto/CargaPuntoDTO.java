package py.com.backend.sfc.dto;

public class CargaPuntoDTO {
	
	private Integer clienteId;
	private Integer monto;
	
	public CargaPuntoDTO(){
		
	}
	
	public Integer getClienteId() {
		return clienteId;
	}
	public void setClienteId(Integer clienteId) {
		this.clienteId = clienteId;
	}
	public Integer getMonto() {
		return monto;
	}
	public void setMonto(Integer monto) {
		this.monto = monto;
	}
	
	
	

}
