package py.com.backend.sfc.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="uso_puntos")
public class UsoPuntos implements Serializable{
	private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_uso")
    @GeneratedValue(generator="usoSec")
    @SequenceGenerator(name="usoSec",sequenceName="uso_sec",allocationSize=0)
    private Integer idUso;
    @JoinColumn(name="id_cliente", referencedColumnName="id_cliente")
	@ManyToOne(optional=false)
	private Cliente idCliente;
    @Basic(optional = false)
    @Column(name = "p_utilizados")
    private Integer puntosUtilizados;
    @Column(name="fecha")
    @Temporal(TemporalType.DATE)
	private Date fecha;
    @Basic(optional = false)
    @Column(name = "id_usuario",length=50)
    private String idUsuario;
    @Basic(optional = false)
    @Column(name = "marca_tiempo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date marcaTiempo;
    @JoinColumn(name="id_concepto", referencedColumnName="id_concepto")
    @ManyToOne(optional=false)
    private ConceptoPuntos idConcepto;
    @OneToMany(cascade=(CascadeType.ALL),mappedBy="idUso")
    @JsonIgnore
    private List<UsoPuntosDetalle> detalles;
    
    public List<UsoPuntosDetalle> getDetalles() {
		return detalles;
	}
	public void setDetalles(List<UsoPuntosDetalle> detalles) {
		this.detalles = detalles;
	}
	public UsoPuntos() {
    	
    }
	public Integer getIdUso() {
		return idUso;
	}
	public void setIdUso(Integer idUso) {
		this.idUso = idUso;
	}
	public Cliente getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Cliente idCliente) {
		this.idCliente = idCliente;
	}
	public Integer getPuntosUtilizados() {
		return puntosUtilizados;
	}
	public void setPuntosutilizados(Integer puntosUtilizados) {
		this.puntosUtilizados = puntosUtilizados;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public ConceptoPuntos getIdConcepto() {
		return idConcepto;
	}
	public void setIdConcepto(ConceptoPuntos idConcepto) {
		this.idConcepto = idConcepto;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Date getMarcaTiempo() {
		return marcaTiempo;
	}
	public void setMarcaTiempo(Date marcaTiempo) {
		this.marcaTiempo = marcaTiempo;
	}
	
}
