package py.com.backend.sfc.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import py.com.backend.sfc.ejb.VencimientoPuntosDAO;
import py.com.backend.sfc.modelo.VencimientoPuntos;

@Path("vencimientoPuntos")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class VPRest {
	@Inject
	private VencimientoPuntosDAO vencimientoPuntosDAO;
	@Context
	protected UriInfo uriInfo;
	@Context
	private HttpServletRequest request;
	
	
	@GET
	@Path("/")
	public Response listar() throws WebApplicationException{
		
		List<VencimientoPuntos> listEntity = null;
		Long total = null;
		total = vencimientoPuntosDAO.total();
		listEntity = vencimientoPuntosDAO.lista();
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		return Response.ok(mapaResultado).build();
		
	}
	
	@GET
	@Path("/{pk}")
	public Response obtener(@PathParam("pk") Integer pk) {
		VencimientoPuntos entityRespuesta =null;
		entityRespuesta = vencimientoPuntosDAO.get(pk);
		return Response.ok(entityRespuesta).build();
	}

	@POST
	@Path("/")
	public Response crear(VencimientoPuntos entity) throws WebApplicationException {
		entity.setIdUsuario(request.getUserPrincipal().getName());
		entity.setMarcaTiempo(new Timestamp(System.currentTimeMillis()));
		vencimientoPuntosDAO.persist(entity);
		
		UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
				.getAbsolutePath());
		URI resourceUri=null;
		try {
			resourceUri = resourcePathBuilder
					.path(URLEncoder.encode(entity.getIdVencimiento().toString(), "UTF-8")).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.created(resourceUri).build();
	}

	@PUT
	@Path("/")
	public Response modificar(VencimientoPuntos entity) throws WebApplicationException {
		entity.setIdUsuario(request.getUserPrincipal().getName());
		entity.setMarcaTiempo(new Timestamp(System.currentTimeMillis()));
		vencimientoPuntosDAO.merge(entity);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{pk}")
	public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {
		
		vencimientoPuntosDAO.delete(pk);
		return Response.ok().build();

	}
}
