package py.com.backend.sfc.rest;

import java.net.URI;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import py.com.backend.sfc.dto.CargaPuntoDTO;
import py.com.backend.sfc.ejb.BolsaPuntosDAO;
import py.com.backend.sfc.ejb.ClienteDAO;
import py.com.backend.sfc.ejb.ReglaPuntosDAO;
import py.com.backend.sfc.ejb.VencimientoPuntosDAO;
import py.com.backend.sfc.modelo.BolsaPuntos;
import py.com.backend.sfc.modelo.Cliente;
import py.com.backend.sfc.modelo.ExcelBP;
import py.com.backend.sfc.modelo.ReporteBP;
import py.com.backend.sfc.modelo.ReporteUP;

@Path("bolsaPuntos")
@Produces("application/json")
@Consumes("application/json")
@RequestScoped
public class BPRest {
	@Inject
	private BolsaPuntosDAO bolsaPuntosDAO;
	@Inject
	private ClienteDAO clienteDAO;
	@Inject
	private VencimientoPuntosDAO VencimientoPuntosDAO;
	@Inject
	private ReglaPuntosDAO reglaPuntosDAO;
	@Context
	protected UriInfo uriInfo;
	@Context
	private HttpServletRequest request;
	
	
	@GET
	@Path("/")
	public Response listar() throws WebApplicationException{
		
		List<BolsaPuntos> listEntity = null;
		Long total = null;
		total = bolsaPuntosDAO.total();
		listEntity = bolsaPuntosDAO.lista();
		Map<String,Object> mapaResultado=new HashMap<String, Object>();
		mapaResultado.put("total", total);
		mapaResultado.put("lista", listEntity);
		
		try {
			ReporteBP.crear(listEntity);
			ExcelBP.crear(listEntity);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok(mapaResultado).build();
		
	}
	
	@GET
	@Path("/{pk}")
	public Response obtener(@PathParam("pk") Integer pk) {
		BolsaPuntos entityRespuesta =null;
		entityRespuesta = bolsaPuntosDAO.get(pk);
		return Response.ok(entityRespuesta).build();
	}

	@POST
	@Path("/")
	public Response crear(CargaPuntoDTO dto) throws WebApplicationException {
		
		BolsaPuntos bolsa = new BolsaPuntos();
		Cliente cliente = clienteDAO.get(dto.getClienteId());
		if(cliente == null) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		bolsa.setIdCliente(cliente);
		bolsa.setFechaAsignacion(new Date());
		bolsa.setMontoOperacion(dto.getMonto());
		bolsa.setPuntosUtilizados(0);
		bolsa.setFechaCaducidad(VencimientoPuntosDAO.get(1).getFinValidez());
		Integer equivalencia = (Integer)reglaPuntosDAO.get(1).getEquivalencia();
		Integer puntos = dto.getMonto() / equivalencia;
		bolsa.setPuntosAsignados(puntos);
		bolsa.setSaldo(puntos);
		bolsa.setIdUsuario(request.getUserPrincipal().getName());
		bolsa.setMarcaTiempo(new Timestamp(System.currentTimeMillis()));
		
		bolsaPuntosDAO.persist(bolsa);
		
		UriBuilder resourcePathBuilder = UriBuilder.fromUri(uriInfo
				.getAbsolutePath());
		URI resourceUri=null;
		try {
			resourceUri = resourcePathBuilder
					.path(URLEncoder.encode(bolsa.getIdBolsa().toString(), "UTF-8")).build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.created(resourceUri).build();
	}

	@PUT
	@Path("/")
	public Response modificar(BolsaPuntos entity) throws WebApplicationException {
		entity.setIdUsuario(request.getUserPrincipal().getName());
		entity.setMarcaTiempo(new Timestamp(System.currentTimeMillis()));
		bolsaPuntosDAO.merge(entity);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{pk}")
	public Response borrar(@PathParam("pk") Integer pk) throws WebApplicationException {
		
		bolsaPuntosDAO.delete(pk);
		return Response.ok().build();

	}
}
